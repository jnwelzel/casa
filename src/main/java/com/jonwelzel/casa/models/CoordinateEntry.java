package com.jonwelzel.casa.models;

import java.util.AbstractMap.SimpleEntry;

public class CoordinateEntry extends SimpleEntry<Integer, Integer> {

	private static final long serialVersionUID = 1L;

	public CoordinateEntry(int i, int j) {
		super(i, j);
	}

}
