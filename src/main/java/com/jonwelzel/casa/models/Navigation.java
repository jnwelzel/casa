package com.jonwelzel.casa.models;

import java.util.HashMap;
import java.util.Map;

public class Navigation {

	private CoordinateEntry[][] map = new CoordinateEntry[5][5];
	private String instructions;
	private Position position;
	private Map<String, Map<String, String>> compass = new HashMap<String, Map<String, String>>();

	public Navigation(String instructions) {
		this.instructions = instructions.toUpperCase();
		int k = 4;
		int l = 0;

		// Initialize map
		for (int i = 0; i <= 4; i++) {
			for (int j = 0; j <= 4; j++) {
				map[i][j] = new CoordinateEntry(l, k);
				l++;
			}
			l = 0;
			k--;
		}

		//Initialize compass
		Map<String, String> north = new HashMap<>();
		north.put("L", "W");
		north.put("R", "E");
		Map<String, String> south = new HashMap<>();
		south.put("L", "E");
		south.put("R", "W");
		Map<String, String> east = new HashMap<>();
		east.put("L", "N");
		east.put("R", "S");
		Map<String, String> west = new HashMap<>();
		west.put("L", "S");
		west.put("R", "R");
		compass.put("N", north);
		compass.put("S", south);
		compass.put("E", east);
		compass.put("W", west);
	}

	public static void main(String[] args) {
		Navigation n = new Navigation();
		System.out.println(n.getMap()[4][2]);
	}

	public Navigation() {
	}

	private CoordinateEntry[][] getMap() {
		return map;
	}

	public String getInstructions() {
		return instructions;
	}

	public Position getFinalPosition() {
		return position;
	}

	public void navigate() {
		String[] steps = instructions.split("");
		// Map coordinate
		CoordinateEntry current = new CoordinateEntry(4, 0);
		// Final result
		Position _position = new Position(map[current.getKey()][current.getValue()], "N");
		for (String s : steps) {
			if (s.equals("L") || s.equals("R")) {
				// Turn
				_position.setDirection(turn(_position.getDirection(), s));
			} else if (s.equals("M")) {
				// Move
				current = move(current, _position.getDirection());
				if (current != null) {
					_position.setCoordinate(map[current.getKey()][current.getValue()]);
				} else {
					break;
				}
			}
		}
		position = _position;
	}

	private String turn(String currentDirection, String leftOrRight) {
		return compass.get(currentDirection).get(leftOrRight);
	}

	private CoordinateEntry move(CoordinateEntry current, String direction) {
		int x = current.getKey();
		int y = current.getValue();

		if (direction.equals("N")) {
			x--;
		} else if (direction.equals("S")) {
			x++;
		} else if (direction.equals("E")) {
			y++;
		} else {
			y--;
		}

		return (x >= 0 && x < 5 && y >= 0 && y < 5) ? new CoordinateEntry(x, y) : null;
	}

}
