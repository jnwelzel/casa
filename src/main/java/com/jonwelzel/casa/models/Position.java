package com.jonwelzel.casa.models;

public class Position {
	private CoordinateEntry coordinate;
	private String direction;

	public Position(CoordinateEntry coordinate, String direction) {
		super();
		this.coordinate = coordinate;
		this.direction = direction;
	}

	public CoordinateEntry getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(CoordinateEntry coordinate) {
		this.coordinate = coordinate;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
