package com.jonwelzel.casa.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jonwelzel.casa.models.Navigation;

@Path("navigation")
@Consumes(MediaType.APPLICATION_JSON)
public class NavigationResource {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Produces(MediaType.TEXT_PLAIN)
	@Path("ping")
	@GET
	public String ping() {
		log.info("Pinging server...");
		return "pong!";
	}

	@Produces(MediaType.APPLICATION_JSON)
	@Path("{instructions}")
	@POST
	public Navigation navigate(@PathParam("instructions") String instructions) {
		// TODO Validate instructions?
		Navigation nav = new Navigation(instructions);
		nav.navigate();
		return nav;
	}

}
