package com.jonwelzel.casa.resources;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("rest")
public class ApplicationResource extends ResourceConfig {

	public ApplicationResource() {
		packages("com.jonwelzel.casa.resources");
		register(JacksonFeature.class);
	}
}
