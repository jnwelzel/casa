<html>
<body>
    <h2>CASA Robot Navigation Testing Application</h2>
    <p><a href="rest/navigation/ping">[GET] Ping? - Ping the server</a>
    <p>[POST] rest/navigation/:instructions - Send navigation instructions (<i>i.e.</i> MML)</p>
    Options
    <ul>
      <li>M - move</li>
      <li>L - turn left</li>
      <li>R - turn right</li>
    </ul>
</body>
</html>
