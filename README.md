#CASA Robots
Navigation system for interplanetary exploration robots.

##Install
Use Maven to generate the `war` package and deploy it to the servlet container of your choice (servlet 3+). Tested on Jetty 9 and Glassfish 4.1 too. Navigate to `http://host:port/casa/` for further instructions.

##P.S.
Does not and will not work on stupid JBoss. Use `mvn jetty:run` or Glassfish.